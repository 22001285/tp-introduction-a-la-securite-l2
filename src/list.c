#include <stdlib.h>

struct cell_t {
  void* val;
  unsigned long int id;
  struct cell_t* next;
};

typedef struct cell_t* list_t;

list_t list_empty(){
	return NULL;
}

int list_is_empty(list_t l){
	return l == NULL;
}

list_t list_push(list_t l, void* x){
	list_t new = malloc(sizeof(struct cell_t));
	new->val = x;
	if (list_is_empty(l)==1){
		new->id = 1;
	}else{
		new->id = 1 + l->id;
	}
	new->next = l;
	return new;
}

list_t list_tail(list_t l){
	return l->next;
}

void* list_pop(list_t* l){
	list_t tmp = (*l)->next;
	void* res = (*l)->val;
	free(*l);
	*l = tmp;
	return res;

}

void* list_top(list_t l){
	return l->val;
}

void list_destroy(list_t l, void (*free_void)(void*)){
	while(list_is_empty(l) == 0){
		free_void(list_pop(&l));
	}
}

// return the found element or NULL
void* list_in(list_t l, void* x, int (*eq)(void*, void*)){
	while (list_is_empty(l)==0){
		if(eq(l->val,x)==1){
			return l->val;
		}
		l = list_tail(l);
	}
	return NULL;
}

unsigned long int list_len(list_t l){
	return l->id;
}
